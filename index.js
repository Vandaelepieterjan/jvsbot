require('dotenv').config();
const request = require('request');
const cron = require('cron');

const db = require('./db.js');
const MAX = 3;

const Discord = require('discord.js');
const client = new Discord.Client();
const apodJob = cron.job('0 0 11 * * *', () => displayAPOD());
const dbJob = cron.job('0 0 23 * * *', () => db.clearUserExec());
apodJob.start();
dbJob.start();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    if (!msg.author.bot && msg.content.substring(0, 1) === '!') {

        if (msg.author.id !== process.env.SUPERADMIN_ID) {
            db.getUserExecutions(msg.author.id).then(res => {
                if (res.length === 0) {
                    db.insertUser(msg.author.id).catch(err => console.log(err));
                    execCmd(msg);
                } else {
                    if (res[0].executions < MAX) {
                        execCmd(msg);
                    } else if (res[0].executions === MAX) {
                        msg.reply(`Je gebruikte de JVSBot al ${res[0].executions} keer.
                    Ga buiten naar de zon, de wolken of de sterren kijken.`)
                    }
                    db.incExecutions(msg.author.id).catch(err => console.log(err));
                }
            });
        } else {
            execCmd(msg);
        }
    }
});

function execCmd(msg) {
    const cmd = msg.content.substring(1);

    switch (cmd) {
        case 'ping':
            msg.reply('pong');
            break;
        case 'bot':
            msg.reply('Ik ben een bot die ontwikkeld wordt door Arne Dierickx.');
            break;
        case 'dadjoke':
            request('https://icanhazdadjoke.com/', {json: true}, (err, res, body) => {
                if (err) {
                    return console.log(err);
                }
                msg.reply(body.joke);
            });
            break;
        case 'helo':
            const random = getRandomInt(1, 4);
            const heloAttachment = new Discord.MessageAttachment(`img/helo${random}.jpg`);
            msg.channel.send(heloAttachment);
            break;
        case 'sputnik':
            getRandSputnikImage(msg);
            break;
        case 'join':
            client.channels.fetch("693070672410312724")
                .then(channel => {
                    channel.join();
                });
            break;
        case 'toekomst':
            const toekomstAttachment = new Discord.MessageAttachment(`img/toekomst.png`);
            msg.channel.send(toekomstAttachment);
            break;
        default:
            msg.reply("Dit is geen geldig commando. Nieuwe features kunnen op deze pagina aangevraagd worden:" +
                " https://gitlab.com/pingudino98/jvsbot/-/issues.");
    }
}

client.login(process.env.DISCORD_TOKEN);

function displayAPOD() {
    request(`https://api.nasa.gov/planetary/apod?api_key=${process.env.APOD_KEY}`, {json: true}, (err, res, body) => {
        if (err) {
            return console.log(err);
        }
        client.channels.fetch('690293411114385408')
            .then(channel => {
                let urlName;
                if (body.hasOwnProperty('hdurl')) {
                    urlName = 'hdurl'
                } else {
                    urlName = 'url';
                }

                let attachment;
                if (body.media_type === 'video') {
                    attachment = new Discord.MessageEmbed().setTitle('Klik hier voor video').setURL(body[urlName]);
                } else {
                    attachment = new Discord.MessageAttachment(body[urlName])
                }

                channel.send(`Astronomy Picture of the Day (NASA)\n${body.title}\n${body.explanation}\nCopyright: ${body.copyright}`);
                channel.send(attachment);
            }).catch(err => console.log(err));
    });
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getRandSputnikImage(msg) {
    request('https://sputnik.jvs-descartes.org/api/observatie', {json: true}, (err, res, body) => {
        if (err) {
            return console.log(err);
        }

        const observatie = body.data[getRandomInt(body.from - 1, body.to)];
        const imagePaths = observatie.afbeeldingen;
        const images = imagePaths.map(path => `https://sputnik.jvs-descartes.org/storage/${path}`);

        msg.channel.send({files: images});
    });
}