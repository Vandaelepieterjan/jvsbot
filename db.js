const {Pool} = require('pg');

const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
    ssl: {
        rejectUnauthorized: false
    }
});

async function getUserExecutions(userId) {
    const {rows} = await pool.query('SELECT executions FROM UserExecutions WHERE userId = $1;', [userId]);
    return rows;
}

async function insertUser(userId) {
    await pool.query('INSERT INTO UserExecutions VALUES($1,1)', [userId]);
}

async function incExecutions(userId) {
    await pool.query('UPDATE UserExecutions SET executions = executions + 1 WHERE userId = $1', [userId]);
}

async function clearUserExec() {
    await pool.query('DELETE FROM UserExecutions');
}

module.exports = {
    getUserExecutions,
    insertUser,
    incExecutions,
    clearUserExec
};